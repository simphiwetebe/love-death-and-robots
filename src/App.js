import { ScrollControls, Scroll, Environment, Sparkles } from '@react-three/drei'
import './App.css';
import { RobotModel } from './components/RobotModel';

function App() {
  
  return (
      <>
      <color attach={'background'} args={['#000']}/>
      <ambientLight intensity={0.1}/>
      <spotLight 
        position={[0,25,0]}
        angle={1.3}
        penumbra={1}
        castShadow
        intensity={1.2}
        shadowBias={-0.0001}
      />
      <Environment
        background={false}
        preset='warehouse'
      />
      <ScrollControls pages={4} damping={0.1}>
        
        <RobotModel scale={0.2} position={[0,-1,0]}/>
        <Sparkles size={2} color={'#fff'} scale={[10,10,10]}/>
      
        <Scroll html style={{width: '100%'}}>
          <div className='section intro'>
            <h3 className='large_text'>K-VRC</h3>
            <h3 className='small_text'>Love, Death + Robots</h3>
          </div>
          <div className='section'>
            <h2 className='title'>Appearance</h2>
            <p className='text'>K-VRC is a diminutive orange-painted robot, with a screen face that projects human-like emotive faces according to his mood. He has two white stripes that are slightly worn out on his head, running from the bill to the back of his head. He also has two antennae on the side of his head which he can control himself. K-VRC has an object on his back resembling a backpack.</p>
          </div>
          <div className='section right'>
            <h2 className='title'>Personality</h2>
            <p className='text'>He is shown to be the most cheerful out of the three robots, showing excitement and enthusiasm for many things. He can also be childish as he really wants to see a basketball ball in action and jokes about a video game console being XBOT 4000’s ancestor. K-VRC is oddly excited by dark things such as blood pits, mass burnings, etc.</p>
          </div>
          <div className='section outro'>
            <button>Buy now</button>
            <p>This work is based on "K-VRC | Love, Death + Robots" 
              <a href="https://sketchfab.com/3d-models/k-vrc-love-death-robots-1a89dbad3a894642958405728ba66d9d">(https://sketchfab.com/3d-models/k-vrc-love-death-robots-1a89dbad3a894642958405728ba66d9d)</a> 
              by ArbitraryCanary <a href="https://sketchfab.com/ArbitraryCanary">(https://sketchfab.com/ArbitraryCanary)</a> licensed under CC-BY-4.0 
              <a href="http://creativecommons.org/licenses/by/4.0/">(http://creativecommons.org/licenses/by/4.0/)</a>
            </p>
          </div>
        </Scroll>
      </ScrollControls>
      </>
  );
}

export default App;
